$(window).on("load", function () {
    const check = $("#end-check");
    const col = $("#end-col");

    function dateVisibility(visibility) {
        if (visibility === 'hidden') {
            if(!col.hasClass("hidden")) {
                col.addClass("hidden");
            }
        } else {
            if(col.hasClass("hidden")) {
                col.removeClass("hidden");
            }
        }
    }

    check.on("change", function () {
        if (document.querySelector('#end-check:checked') !== null) {
            dateVisibility("visible")
        } else {
            dateVisibility("hidden")
        }
    })
});