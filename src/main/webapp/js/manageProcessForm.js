$(window).on("load", function () {
    console.log("loaded");
    const fileSelector = document.getElementById("fileSelector");
    const warning = $("#csv-warning");
    const check = $("#processCheck");
    const separatorLabel = $("#separator");

    function separatorVisibility(visibility) {
        if (visibility === 'hidden') {
            if(!separatorLabel.hasClass("hidden")) {
                separatorLabel.addClass("hidden");
            }
        } else {
            if(separatorLabel.hasClass("hidden")) {
                separatorLabel.removeClass("hidden");
            }
        }
    }
    function manageVisibilityCsv() {
        if(!warning.hasClass("hidden")) {
            warning.addClass("hidden");
        }
        check.removeAttr("disabled");
    }

    function manageVisibilityNonCsv() {
        if(warning.hasClass("hidden")) {
            warning.removeClass("hidden");
        }
        check.attr("disabled", "disabled");
        check.removeAttr("checked");

        if(!separatorLabel.hasClass("hidden")) {
            separatorLabel.addClass("hidden");
        }
    }

    function manageVisibility() {
        const selected = $("#fileSelector :selected").val();
        if (selected.endsWith(".csv")) {
            manageVisibilityCsv()
        } else {
            manageVisibilityNonCsv()
        }
    }

    manageVisibility();
    fileSelector.addEventListener("change", manageVisibility)
    check.on("change", function () {
        if (document.querySelector('#processCheck:checked') !== null) {
            separatorVisibility("visible")
        } else {
            separatorVisibility("hidden")
        }
    })
})