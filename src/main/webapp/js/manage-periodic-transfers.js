$(window).on("load", function () {
    // buttons to press for submitting forms
    const createSubmit = $("#create-new-submit")
    const createButton = $("#create-new-btn")
    const modifySubmit = $("#modify-submit")
    const modifyButton = $("#modify-btn")

    createButton.click(function () {
        createSubmit.trigger("click");
    })

    modifyButton.click(function () {
        modifySubmit.trigger("click")
    })

    // adding entries to periodic form
    let addButton = $("#add-new-now-new");
    const addForm = $("#add-form");
    addButton.on("click", function () {
        addNewFormEntry(addButton, addForm, "add-new-now-new");
    })

    let modifyFormButton = $("#add-new-now-modify");
    const modifyForm = $("#modify-form");
    modifyFormButton.on("click", function () {
        addNewFormEntry(modifyFormButton, modifyForm, "add-new-now-modify");
    })

    function addNewFormEntry(btn, form, id) {
        let hiddenRow = $("#next-form-row");
        let nextAddButton = $("#add-new-next");
        // clone hidden row so nothing changes
        let cln = hiddenRow.clone();
        // make original hidden row visible
        hiddenRow.removeClass("hidden")
        hiddenRow.removeAttr("id");
        form.append(cln);
        // original button is hidden
        btn.removeAttr("id");
        btn.addClass("hidden");
        // make the last button in hidden row the current button
        btn = nextAddButton;
        btn.attr("id", id);
        btn.on("click", function () {
            addNewFormEntry(btn, form, id);
        });
    }
})