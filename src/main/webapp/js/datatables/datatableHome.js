$(window).on("load", function () {
    const table = $('#datatable').DataTable({
        rowGroup: {
            dataSrc: 0
        },
    });
    table.order( [0, 'asc' ]).draw()
})