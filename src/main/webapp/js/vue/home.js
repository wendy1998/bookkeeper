if (document.readyState) {
    new Vue( {
        el: '#vue',
        data: {
            menuItems: [
                {
                    name: 'Overview',
                    page: 'overview',
                    active: true
                },
                {
                    name: 'File processing',
                    page: 'fileprocessor.jsp',
                    active: false
                },
                {
                    name: "Add custom transaction",
                    page: "addTransaction.jsp",
                    active: false
                }
            ]
        }
    });
}