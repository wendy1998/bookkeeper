package nl.wendy.bookkeeper.FileProcessing;

import com.google.gson.Gson;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class FileProcessor {
    public static void addNewTypeToSnapshot(String type) throws Exception {
        try {
            HashMap<String, String> snapshot = FileProcessor.loadJsonFile(false);
            snapshot.put(type, "0");
            FileProcessor.saveJsonFiles(snapshot, false);
        } catch (Exception e) {
            throw new Exception("FileProcessor - loadPeriodicTransactions: " + e.getMessage());
        }
    }
    public static HashMap<String, String> loadJsonFile(boolean periodic) throws Exception {
        try {
            Context env = (Context) new InitialContext().lookup("java:comp/env");
            String filePath = (String) env.lookup("file-save");
            String filename;
            if (periodic) {
                filename = "periodic_transactions.json";
            } else {
                filename = "snapshot.json";
            }
            File file = new File(filePath + "/" + filename);
            if (file.exists()) {
                try (FileReader reader = new FileReader(file)) {
                    JSONParser parser = new JSONParser();
                    return new Gson().fromJson(
                            parser.parse(reader).toString(),
                            HashMap.class);
                }
            }
            return null;
        } catch (Exception e) {
            throw new Exception("FileProcessor - loadJsonFile: " + e.getMessage());
        }
    }

    public static void saveJsonFiles(HashMap<String, String> elements,
                                     boolean periodic) throws Exception {
        JSONObject json = new JSONObject(elements);
        FileWriter writer = null;
        try {
            String filename;
            if (periodic) {
                filename = "periodic_transactions.json";
            } else {
                filename = "snapshot.json";
            }
            Context env = (Context) new InitialContext().lookup("java:comp/env");
            String filePath = (String) env.lookup("file-save");
//            String filePath = "C:\\Users\\wendy\\Documents\\websites\\bookkeeper\\data\\saved";
            writer = new FileWriter(new File(filePath + "/" + filename));
            writer.write(json.toString());
        } catch (Exception e) {
            throw new Exception("FileProcessor - saveJsonFile: " + e.getMessage());
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }
    public static ArrayList<String> saveTransactionsFiles(List<FileItem> fileItems) throws Exception {
        ArrayList<String> unprocessed = new ArrayList<>();
        try {
            Context env = (Context) new InitialContext().lookup("java:comp/env");
            String filePath = (String) env.lookup("file-upload");
            for (FileItem fileItem : fileItems) {
                if (!fileItem.isFormField()) {
                    // Get the uploaded file parameters
                    String fileName = fileItem.getName();

                    // Write the file
                    File file;
                    if (fileName.lastIndexOf("/") >= 0) {
                        file = new File(filePath + "/" +
                                fileName.substring(fileName.lastIndexOf("/")));
                    } else {
                        file = new File(filePath + "/" +
                                fileName.substring(fileName.lastIndexOf("/") + 1));
                    }
                    try {
                        fileItem.write(file);
                    } catch (Exception e) {
                        unprocessed.add(fileName);
                    }
                }
            }
        } catch (NamingException e) {
            throw new Exception("FileProcessor - saveTransactionsFiles: namingException: " + e.getMessage());
        }
        return unprocessed;
    }

    public static void cleanFiles() throws Exception {
        try {
            Context env = (Context) new InitialContext().lookup("java:comp/env");
            String filePath = (String) env.lookup("file-upload");
            FileUtils.cleanDirectory(new File(filePath));
        } catch (NamingException e) {
            throw new Exception("FileProcessor - cleanFiles: namingException: " + e.getMessage());
        }
    }

    public static ArrayList<String> getFiles() throws Exception {
        ArrayList<String> out = new ArrayList<>();
        try {
            Context env = (Context) new InitialContext().lookup("java:comp/env");
            String filePath = (String) env.lookup("file-upload");
            File[] files = new File(filePath).listFiles();
            if (files != null) {
                for (File file: files) {
                    if (file.isFile()) {
                        out.add(file.getName());
                    }
                }
            }
            return out;
        } catch (NamingException e) {
            throw new Exception("FileProcessor - getFiles: namingException: " + e.getMessage());
        }
    }

    public static void removeFile(String filename) throws Exception {
        try {
            Context env = (Context) new InitialContext().lookup("java:comp/env");
            String filePath = (String) env.lookup("file-upload");
            new File(filePath + "\\" + filename).deleteOnExit();
        } catch (NamingException e) {
            throw new Exception("FileProcessor - removeFile: namingException: " + e.getMessage());
        }
    }
}
