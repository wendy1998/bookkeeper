package nl.wendy.bookkeeper.FileProcessing;

import nl.wendy.bookkeeper.objects.MemoryTransaction;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class CsvProcessor {
    private static final int FROM_IDX = 1;
    private static final int DATE_IDX = 0;
    private static final int TO_IDX = 3;
    private static final int AMOUNT_IDX = 10;
    private static final int DESCRIPTION_IDX = 17;

    public ArrayList<MemoryTransaction> loadTransactions(String filename, String separator) throws Exception {
        BufferedReader csvReader = null;
        try {
            ArrayList<MemoryTransaction> transactions = new ArrayList<>();
            Context env = (Context) new InitialContext().lookup("java:comp/env");
            String filePath = (String) env.lookup("file-upload");
            csvReader = new BufferedReader(new FileReader(filePath + "/" + filename));
            String row;
            while ((row = csvReader.readLine()) != null) {
                String[] data = row.split(separator);
                // do something with the data
                LocalDate date = LocalDate.parse(data[DATE_IDX].replaceAll("\"", " ").strip(),
                        DateTimeFormatter.ofPattern("dd-MM-yyyy"));
                MemoryTransaction transaction = new MemoryTransaction(
                        data[FROM_IDX].replace('"', ' ').strip(),
                        data[TO_IDX].replace('"', ' ').strip(),
                        date,
                        Double.parseDouble(data[AMOUNT_IDX].replace('"', ' ').strip()),
                        data[DESCRIPTION_IDX].replace('"', ' ').strip());
                transactions.add(transaction);
            }
            return transactions;
        } catch (NamingException e) {
            throw new Exception("CsvProcessor - loadTransactions: namingException: " + e.getMessage());
        } catch (FileNotFoundException e) {
            throw new Exception("CsvProcessor - loadTransactions: FileNotFoundException: " + e.getMessage());
        } catch (IOException e) {
            throw new Exception("CsvProcessor - loadTransactions: IOException: " + e.getMessage());
        } finally {
            if(csvReader != null) {
                csvReader.close();
            }
        }
    }
}
