package nl.wendy.bookkeeper.dao;

import nl.wendy.bookkeeper.objects.DatabaseTransaction;
import nl.wendy.bookkeeper.objects.MemoryTransaction;

import javax.naming.Context;
import javax.naming.InitialContext;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static nl.wendy.bookkeeper.dao.DatabaseConnectable.cleanUp;

public class ConnectionManager implements SqlManager {
    private static ConnectionManager singletonInstance = null;

    /**
     * Private constructor so only singletonInstance can be used.
     */
    private ConnectionManager() {};

    /**
     * Make sure only one instance of this class exists in this program.
     * @return The single instance of this class
     */
    public static ConnectionManager defaultManager() {
        if (singletonInstance == null) {
            singletonInstance = new ConnectionManager();
        }
        return singletonInstance;
    }

    // transactions management

    /**
     * Used to check for transactions that are possibly duplicates
     * @param memoryTransaction the transaction to check
     * @return any transactions that could be duplicates
     * @throws DatabaseException if something goes wrong
     */
    public ArrayList<DatabaseTransaction> checkForDuplicates(MemoryTransaction memoryTransaction) throws Exception {
        return checkForDuplicates(memoryTransaction.getAmount(), memoryTransaction.getAccount());
    }

    /**
     * check for duplicates for custom transaction
     * @param amount the amount of money transferred
     * @param account what account the transaction was from
     * @return any transactions that could be duplicates
     * @throws DatabaseException if something goes wrong
     */
    public ArrayList<DatabaseTransaction> checkForDuplicates(double amount, String account) throws Exception {
        String sql = "SELECT * FROM transactions WHERE ? = Amount " +
                "and ? = Account";

        // create parameter map
        HashMap<Integer, List<String>> parameters = new HashMap<>() {{
            put(1, List.of(String.valueOf(amount), "double"));
            put(2, List.of(account.toLowerCase(), "string"));
        }};

        HashMap<String, List<String>> rs = executeQuery(sql, parameters);
        return mapToObjects(rs);
    }

    /**
     * Add a memory transaction to the database
     * @param memoryTransaction the transaction to add
     * @param type what type of transaction it is
     * @param tag what description to tag it with in database
     * @throws DatabaseException if something goes wrong
     */
    public void addNewTransaction(MemoryTransaction memoryTransaction, String type, String tag) throws Exception {
        addNewTransaction(
            memoryTransaction.getDateString(),
            memoryTransaction.getAmount(),
            memoryTransaction.getAccount(),
            type,
            tag
        );
    }

    /**
     * Add a custom transaction to database
     * @param date the date the transaction happened
     * @param amount the amount of money transferred
     * @param account the account on which it happened
     * @param type what type of transaction it is
     * @param tag what description to tag it with in database
     * @throws DatabaseException if something goes wrong
     */
    public void addNewTransaction(String date, double amount, String account, String type, String tag) throws Exception {
        String sql = "INSERT INTO `transactions` (`Date`, `Amount`, `Account`, `Type`, `Tag`) VALUES (?, ?, ?, ?, ?);";
        // create parameter map
        HashMap<Integer, List<String>> parameters = new HashMap<>() {{
            put(1, List.of(date, "date"));
            put(2, List.of(String.valueOf(amount), "double"));
            put(3, List.of(account.toLowerCase(), "string"));
            put(4, List.of(type, "string"));
            put(5, List.of(tag, "string"));
        }};
        executeUpdate(sql, parameters);
    }

    public void modifyTransaction(int id, String date, double amount, String account, String type, String tag) throws Exception {
        removeTransaction(id);
        addNewTransaction(date, amount, account, type, tag);
    }

    public void removeTransaction(int id) throws Exception {
        String sql = "DELETE FROM `transactions` WHERE (`ID` = ?);";
        HashMap<Integer, List<String>> parameters = new HashMap<>() {{
            put(1, List.of(String.valueOf(id), "int"));
        }};
        executeUpdate(sql, parameters);
    }

    /**
     * Load all the transactions from a specific date onwards
     * @param startDate the date from which to load the transactions
     * @return the transactions
     * @throws DatabaseException if something goes wrong
     */
    public ArrayList<DatabaseTransaction> loadTransactions(LocalDate startDate) throws Exception {
        return loadTransactions(startDate, null);
    }

    /**
     * Load all the transactions between two dates
     * @param startDate the date from which to load the transactions
     * @param endDate the date until which to load the transactions, can be null
     * @return the transactions
     * @throws DatabaseException if something goes wrong
     */
    public ArrayList<DatabaseTransaction> loadTransactions(LocalDate startDate, LocalDate endDate) throws Exception {
        String sql = "SELECT * FROM transactions WHERE ? <= Date";

        // create parameter map
        HashMap<Integer, List<String>> parameters = new HashMap<>() {{
            put(1, List.of(startDate.toString(), "string"));
        }};

        if (endDate != null) {
            sql += " && Date <= ?";
            parameters.put(2, List.of(endDate.toString(), "string"));
        }

        HashMap<String, List<String>> rs = executeQuery(sql, parameters);
        return mapToObjects(rs);
    }

    // transaction type management

    /**
     * Add new transaction type
     * @param name name of the type
     * @param textColor color for the text associated with it
     * @param bgColor color for the background associated with it
     * @throws DatabaseException if something goes wrong
     */
    public void addNewType(String name, String textColor, String bgColor) throws Exception {
        String sql = "INSERT INTO `transactiontype` (`Name`, `TextColor`, `BgColor`) VALUES (?, ?, ?)";
        HashMap<Integer, List<String>> parameters = new HashMap<>() {{
            put(1, List.of(name, "string"));
            put(2, List.of(textColor, "string"));
            put(3, List.of(bgColor, "string"));
        }};
        executeUpdate(sql, parameters);
    }

    /**
     * Remove transaction type
     * @param name name of the type
     * @throws DatabaseException if something goes wrong
     */
    public void removeType(String name) throws Exception {
        String sql = "DELETE FROM `transactiontype` WHERE (`Name` = ?);";
        HashMap<Integer, List<String>> parameters = new HashMap<>() {{
            put(1, List.of(name, "string"));
        }};
        executeUpdate(sql, parameters);
    }

    /**
     * Change the color associated with a transaction type
     * @param name name of the type
     * @param color new color associated with it
     * @throws DatabaseException if something goes wrong
     */
    public void changeColorType(String name, String type, String color) throws Exception {
        String sql = "UPDATE `transactiontype` SET `"
                + (type.equals("background") ? "BgColor" : "TextColor" ) +
                "` = ? WHERE (`Name` = ?)";
        HashMap<Integer, List<String>> parameters = new HashMap<>() {{
            put(1, List.of(color, "string"));
            put(2, List.of(name, "string"));
        }};
        executeUpdate(sql, parameters);
    }

    /**
     * Load all the types
     * @return the types
     * @throws DatabaseException if something goes wrong
     */
    public HashMap<String, String[]> loadTypes() throws Exception {
        HashMap<String, String[]> types = new HashMap<>();
        String sql = "SELECT * FROM `transactiontype`";
        HashMap<String, List<String>> rs = executeQuery(sql, null);
        int numRows = rs.get("Name").size();
        for (int i = 0; i < numRows; i++) {
            types.put(
                    rs.get("Name").get(i),
                    new String[] {
                            rs.get("TextColor").get(i),
                            rs.get("BgColor").get(i)
                    }
            );
        }

        return types;
    }

    // storage type management

    /**
     * Add new storage
     * @param name name of the storage
     * @throws DatabaseException if something goes wrong
     */
    public void addNewStorage(String name) throws Exception {
        String sql = "INSERT INTO `moneystorages` (`Name`) VALUES (?);\n";
        HashMap<Integer, List<String>> parameters = new HashMap<>() {{
            put(1, List.of(name, "string"));
        }};
        executeUpdate(sql, parameters);
    }

    /**
     * remove storage
     * @param name name of the storage
     * @throws DatabaseException if something goes wrong
     */
    public void removeStorage(String name) throws Exception {
        String sql = "DELETE FROM `moneystorages` WHERE (`Name` = ?);";
        HashMap<Integer, List<String>> parameters = new HashMap<>() {{
            put(1, List.of(name, "string"));
        }};
        executeUpdate(sql, parameters);
    }

    /**
     * Load all the storages
     * @return the storages
     * @throws DatabaseException if something goes wrong
     */
    public List<String> loadStorages() throws Exception {
        String sql = "SELECT * FROM `moneystorages`";
        HashMap<String, List<String>> rs = executeQuery(sql, null);
        return rs.get("Name");
    }
}
