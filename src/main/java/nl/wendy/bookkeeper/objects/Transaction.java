package nl.wendy.bookkeeper.objects;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

public abstract class Transaction {
    private final String account;
    private final LocalDate date;
    private final double amount;

    public Transaction(LocalDate date, double amount, String account) {
        this.date = date;
        this.amount = amount;
        this.account = account;
    }

    public static List<Object> setupPeriodicTransactions(String[] amounts, String[] descriptions) throws Exception {
        int skipped = 0;
        HashMap<String, String> out = new HashMap<>(amounts.length);
        for (int i = 0; i < amounts.length; i++) {
            if ((amounts[i].isEmpty() || amounts[i].isBlank()) ||
                    (descriptions[i].isBlank() || descriptions[i].isEmpty())) {
                skipped++;
            } else {
                out.put(
                        descriptions[i].strip(), amounts[i]
                );
            }
        }

        return List.of(skipped, out);
    }

    public String getAccount() {
        return account;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getDateString() {
        return date.getYear() + "-" + date.getMonthValue() + "-" + date.getDayOfMonth();
    }

    public double getAmount() {
        return amount;
    }
}
