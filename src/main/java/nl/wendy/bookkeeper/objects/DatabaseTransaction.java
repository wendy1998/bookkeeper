package nl.wendy.bookkeeper.objects;

import java.time.LocalDate;

public class DatabaseTransaction extends Transaction {
    private final String type;
    private final String tag;
    private final int id;

    public DatabaseTransaction(int id, LocalDate date, double amount, String account, String type, String tag) {
        super(date, amount, account);
        this.type = type;
        this.tag = tag;
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public String getTag() { return tag; }

    public int getId() { return id; }
}
