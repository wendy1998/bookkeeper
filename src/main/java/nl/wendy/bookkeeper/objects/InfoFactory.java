package nl.wendy.bookkeeper.objects;

import nl.wendy.bookkeeper.FileProcessing.FileProcessor;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class InfoFactory {
    private final HashMap<String, String[]> transactionTypes;
    private final List<String> moneyStorages;
    private HashMap<String, String> periodicTransactions;
    private final String beginDate;
    private List<DatabaseTransaction> loadedTransactions = null;
    private Map<String, Object> overviewTransactions = null;
    private HashMap<String, String> moneySnapshot;
    private HashMap<String, Double> calculatedSnapshot;
    private LocalDate snapshotDate = null;
    private String lastDate;

    public InfoFactory() throws Exception {
        beginDate = findDate();
        transactionTypes = TransactionType.getTypes(true);
        moneyStorages = MoneyStorages.getStorages(true);
        periodicTransactions = FileProcessor.loadJsonFile(true);
        if (periodicTransactions != null) {
            this.periodicTransactions.put("total",
                    String.valueOf(
                            periodicTransactions.values().stream()
                                    .mapToDouble(Double::parseDouble)
                                    .sum()
                    )
            );
        }
        moneySnapshot = FileProcessor.loadJsonFile(false);
        setSnapshotDate();
    }

    private void setSnapshotDate() {
        if (moneySnapshot != null) {
            String[] splt = moneySnapshot.get("date").split("-");
            snapshotDate = LocalDate.of(
                    Integer.parseInt(splt[0]),
                    Integer.parseInt(splt[1]),
                    Integer.parseInt(splt[2])
            );
        }
    }

    private HashMap<String, Double> calculateThroughSnapshot() {
        HashMap<String, Double> out = new HashMap<>();
        moneySnapshot.forEach((type, amount) -> {
            if (!type.equals("date")) {
                out.put(type, Double.parseDouble(amount));
            }
        });

        Map<String, List<DatabaseTransaction>> collect = loadedTransactions.stream()
                .filter(databaseTransaction -> databaseTransaction.getDate().isAfter(snapshotDate))
                .collect(Collectors.groupingBy(DatabaseTransaction::getType));

        collect.forEach((type, transactions) -> {
            String snapshotKey = type;
            if (type.equals("periodic")) {
                snapshotKey = "regular";
            }
            double sum = transactions.stream().mapToDouble(Transaction::getAmount).sum();
            out.replace(
                    snapshotKey,
                    out.get(snapshotKey) + sum
            );
        });

        return out;
    }

    private String findDate() {
        // set standard start-date of this month
        LocalDate now = LocalDate.now();
        int month = now.getMonthValue();
        int year = now.getYear();
        int day = now.getDayOfMonth();
        String date;

        // month is from the 20th to the 19th, so if before the 20th,
        // the start date is in the last month
        if (day < 20) {
            month -= 1;
            if(month == 0) {
                year -= 1;
                month = 12;
            }
        }
        String monthStr = String.valueOf(month);
        if (month < 10) {
            monthStr = "0" + monthStr;
        }
        date = year + "-" + monthStr + "-20";

        return date;
    }

    public Map<String, Object> calculateOverview() {
        Map<LocalDate, Map<String, List<DatabaseTransaction>>> grouped =
                this.loadedTransactions.stream()
                        .collect(
                                Collectors.groupingBy(Transaction::getDate,
                                        Collectors.groupingBy(DatabaseTransaction::getType,
                                                Collectors.toList()
                                        )
                                )
                        );
        LocalDate[] dates = grouped.keySet().toArray(LocalDate[]::new);
        Arrays.sort(dates);
        List<List<Integer>> config = calculateOverviewConfig(dates.length);
        this.lastDate = dates[dates.length -1].toString();
        Map<LocalDate, Map<String, Double>> sumsPerDate = calculateSums(grouped);

        Map<String, Object> out = new HashMap<>(4);
        out.put("grouped", grouped);
        out.put("dates", dates);
        out.put("config", config);
        out.put("sums", sumsPerDate);

        return out;
    }

    private Map<LocalDate, Map<String, Double>> calculateSums(Map<LocalDate, Map<String, List<DatabaseTransaction>>> data) {
        Map<LocalDate, Map<String, Double>> sumsPerDate = new HashMap<>();
        data.forEach((date, types) -> {
            Map<String, Double> sums = new HashMap<>();
            types.forEach((type, transactions) -> {
                double sum = transactions.stream().mapToDouble(Transaction::getAmount).sum();
                sums.put(type, sum);
            });
            sumsPerDate.put(date, sums);
        });
        return sumsPerDate;
    }

    private List<List<Integer>> calculateOverviewConfig(int amountOfDates) {
        List<List<Integer>> config = new ArrayList<>();
        int amount = 5;
        if (amountOfDates > 10) {
            amount = (int) Math.ceil(
                    amountOfDates / 2.0
            );
        }
        ArrayList<Integer> indices = new ArrayList<>(amount);
        for (int i = 0; i < amountOfDates; i++) {
            if (indices.size() == amount) {
                config.add(indices);
                indices = new ArrayList<>(amount);
            }
            indices.add(i);
        }

        if (indices.size() > 0) {
            config.add(indices);
        }
        return config;
    }

    public HashMap<String, String[]> getTransactionTypes() {
        return transactionTypes;
    }

    public List<String> getMoneyStorages() {
        return moneyStorages;
    }

    public HashMap<String, String> getPeriodicTransactions() {
        return periodicTransactions;
    }

    public HashMap<String, String> getMoneySnapshot() { return moneySnapshot; }

    public String getBeginDate() {
        return beginDate;
    }

    public List<DatabaseTransaction> getLoadedTransactions() {
        return loadedTransactions;
    }

    public Map<String, Object> getOverviewTransactions() {
        return overviewTransactions;
    }

    public void setLoadedTransactions(List<DatabaseTransaction> loadedTransactions) {
        this.loadedTransactions = loadedTransactions;
        this.overviewTransactions = calculateOverview();
        calculatedSnapshot = calculateThroughSnapshot();
    }

    public void setPeriodicTransactions(HashMap<String, String> periodicTransactions) {
        this.periodicTransactions = periodicTransactions;
        this.periodicTransactions.put("total",
                String.valueOf(
                        periodicTransactions.values().stream()
                                .mapToDouble(Double::parseDouble)
                                .sum()
                )
        );
    }

    public void reloadMoneySnapshot() throws Exception {
        moneySnapshot = FileProcessor.loadJsonFile(false);
        setSnapshotDate();
        calculatedSnapshot = calculateThroughSnapshot();
    }

    public LocalDate getSnapshotDate() {
        return snapshotDate;
    }

    public String getLastDate() {
        return lastDate;
    }

    public HashMap<String, Double> getCalculatedSnapshot() {
        return calculatedSnapshot;
    }
}
