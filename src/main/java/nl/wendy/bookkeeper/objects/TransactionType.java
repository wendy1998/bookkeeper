package nl.wendy.bookkeeper.objects;

import nl.wendy.bookkeeper.FileProcessing.FileProcessor;
import nl.wendy.bookkeeper.dao.ConnectionManager;

import java.util.HashMap;

public abstract class TransactionType {
    private static HashMap<String, String[]> types = new HashMap<>() {
        {
            put("regular", new String[]{"white", "grey"});
            put("periodic", new String[]{"red", "indianred"});
            put("personal", new String[]{"blue", "cadetblue"});
            put("personal_savings", new String[]{"purple", "mediumpurple"});
            put("big_savings", new String[]{"greenyellow", "yellowgreen"});
            put("anniversary", new String[]{"hotpink", "hotpink"});
            put("public_transport", new String[]{"darkgoldenrod", "darkgoldenrod"});
        }};

    public static HashMap<String, String[]> getTypes(boolean reload) throws Exception {
        if (reload) {
            loadFromDatabase();
        }
        return types;
    }

    private static void loadFromDatabase() throws Exception {
        ConnectionManager connectionManager = ConnectionManager.defaultManager();
        TransactionType.types = connectionManager.loadTypes();
    }

    public static void addNewType(String name, String textColor, String bgColor, boolean addToSnapshot) throws Exception {
        if (!types.containsKey(name)) {
            types.put(name, new String[]{
                    textColor, bgColor
            });
            // add to database
            ConnectionManager connectionManager = ConnectionManager.defaultManager();
            connectionManager.addNewType(name, textColor, bgColor);

            if (addToSnapshot) {
                FileProcessor.addNewTypeToSnapshot(name);
            }
        } else {
            throw new IllegalArgumentException(
                    "TransactionType - addNewType: Illegal argument exception, type name " + name + " already exists");
        }
    }

    public static void removeType(String name) throws Exception {
        loadFromDatabase();
        if (!types.containsKey(name)) {
            throw new IllegalArgumentException(
                    "TransactionType - removeType: Illegal argument exception, type name " + name + " doesn't exists");
        } else {
            types.remove(name);
            // remove from database
            ConnectionManager connectionManager = ConnectionManager.defaultManager();
            connectionManager.removeType(name);
        }
    }

    public static void changeColor(String name, String type, String color) throws Exception {
        loadFromDatabase();
        if (!types.containsKey(name)) {
            throw new IllegalArgumentException(
                    "TransactionType - changeColor: Illegal argument exception, type name " + name + " doesn't exists");
        } else {
            if (type.equals("background")) {
                types.get(name)[1] = color;
            } else {
                types.get(name)[0] = color;
            }
            ConnectionManager connectionManager = ConnectionManager.defaultManager();
            connectionManager.changeColorType(name, type, color);
        }
    }
}
