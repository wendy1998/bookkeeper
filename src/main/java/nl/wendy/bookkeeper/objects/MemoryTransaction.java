package nl.wendy.bookkeeper.objects;

import nl.wendy.bookkeeper.dao.ConnectionManager;

import java.time.LocalDate;
import java.util.ArrayList;

public class MemoryTransaction extends Transaction {
    private final String to;
    private final String description;
    private boolean inDatabase = false;

    public MemoryTransaction(String account, String to, LocalDate date, double amount, String description) {
        super(date, amount, account);
        this.to = to;
        this.description = description;
    }

    public void addToDatabase(String type, String tag) throws Exception {
        ConnectionManager manager = ConnectionManager.defaultManager();
        manager.addNewTransaction(this, type, tag);
        this.inDatabase = true;
    }

    public ArrayList<DatabaseTransaction> checkForDuplicates() throws Exception {
        ConnectionManager manager = ConnectionManager.defaultManager();
        return manager.checkForDuplicates(this);
    }

    public String getTo() {
        return to;
    }

    public String getDescription() {
        return description;
    }

    public boolean isInDatabase() {
        return inDatabase;
    }

    public void setInDatabase(boolean inDatabase) {
        this.inDatabase = inDatabase;
    }
}
