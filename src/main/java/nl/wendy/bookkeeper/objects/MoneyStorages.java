package nl.wendy.bookkeeper.objects;

import nl.wendy.bookkeeper.dao.ConnectionManager;

import java.util.List;

public class MoneyStorages {
    private static List<String> storages = List.of(
            "Bankaccount",
            "Cash", "Creditcard",
            "Paypal", "Vouchers");

    public static List<String> getStorages(boolean reload) throws Exception {
        if (reload) {
            loadFromDatabase();
        }
        return storages;
    }

    private static void loadFromDatabase() throws Exception {
        ConnectionManager connectionManager = ConnectionManager.defaultManager();
        storages = connectionManager.loadStorages();
    }

    public static void addNewStorage(String name) throws Exception {
        if (!storages.contains(name)) {
            storages.add(name);

            // add to database
            ConnectionManager connectionManager = ConnectionManager.defaultManager();
            connectionManager.addNewStorage(name);
        } else {
            throw new IllegalArgumentException(
                    "MoneyStorages - addNewStorage: Illegal argument exception, storage name " + name + " already exists");
        }
    }

    public static void removeStorage(String name) throws Exception {
        loadFromDatabase();
        if (!storages.contains(name)) {
            throw new IllegalArgumentException(
                    "MoneyStorages - removeStorage: Illegal argument exception, storage name " + name + " doesn't exists");
        } else {
            storages.remove(name);
            // remove from database
            ConnectionManager connectionManager = ConnectionManager.defaultManager();
            connectionManager.removeStorage(name);
        }
    }
}
