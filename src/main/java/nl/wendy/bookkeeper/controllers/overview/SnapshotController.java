package nl.wendy.bookkeeper.controllers.overview;

import nl.wendy.bookkeeper.FileProcessing.FileProcessor;
import nl.wendy.bookkeeper.controllers.AbstractController;
import nl.wendy.bookkeeper.objects.InfoFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;

public class SnapshotController extends AbstractController {

    public SnapshotController(HttpServletRequest request, HttpServletResponse response) {
        super(request, response);
    }

    public void saveNewSnapshot() throws Exception {
        HttpSession session = this.request.getSession();
        InfoFactory inf = (InfoFactory) session.getAttribute("infoFactory");
        HashMap<String, Double> calculatedSnapshot = inf.getCalculatedSnapshot();
        HashMap<String, String> toSave = new HashMap<>(calculatedSnapshot.size());
        calculatedSnapshot.forEach((type, amount) -> {
            toSave.put(type, String.valueOf(amount));
        });
        toSave.put("date", inf.getLastDate());
        FileProcessor.saveJsonFiles(toSave, false);
        inf.reloadMoneySnapshot();
        this.request.setAttribute("successMessage", "New Snapshot saved.");
    }

    public void hideFromSnapshot() {
        // TODO: hide from snapshot
    }
    public void addTypeToSnapshot() {
        // TODO: add new (snapshot) type
    }
}
