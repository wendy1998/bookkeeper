package nl.wendy.bookkeeper.controllers.overview;

import nl.wendy.bookkeeper.controllers.AbstractController;
import nl.wendy.bookkeeper.dao.ConnectionManager;
import nl.wendy.bookkeeper.objects.DatabaseTransaction;
import nl.wendy.bookkeeper.objects.InfoFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OverviewController extends AbstractController {

    public OverviewController(HttpServletRequest request, HttpServletResponse response) {
        super(request, response);
    }

    private LocalDate[] calculateDateRange() {
        String startDateString = request.getParameter("start-date");
        String[] startSplit = startDateString.split("-");
        LocalDate startDate = LocalDate.of(Integer.parseInt(startSplit[0]),
                Integer.parseInt(startSplit[1]),
                Integer.parseInt(startSplit[2]));


        LocalDate endDate = null;
        if (request.getParameter("use-end-date") != null) {
            String endDateString = request.getParameter("end-date");
            if (endDateString.isEmpty()) {
                request.setAttribute("errorMessage", "end date was empty, so it was ignored.");
            } else {
                String[] endSplit = endDateString.split("-");
                endDate = LocalDate.of(Integer.parseInt(endSplit[0]),
                        Integer.parseInt(endSplit[1]),
                        Integer.parseInt(endSplit[2]));
                if(endDate.isBefore(startDate)) {
                    request.setAttribute("errorMessage", "end date was before start date, so it was ignored.");
                    endDate = null;
                }
            }
        }
        return new LocalDate[]{startDate, endDate};
    }

    public void calculateOverview() throws Exception {
        calculateOverview(true);
    }

    public void calculateOverview(boolean extractDate) throws Exception {
        HttpSession session = this.request.getSession();
        InfoFactory factory = (InfoFactory) session.getAttribute("infoFactory");
        LocalDate startDate;
        LocalDate endDate = null;
        if (extractDate) {
            LocalDate[] dates = calculateDateRange();
            startDate = dates[0];
            endDate = dates[1];
        } else {
            String startDateString = factory.getBeginDate();
            String[] startSplit = startDateString.split("-");
            startDate = LocalDate.of(Integer.parseInt(startSplit[0]),
                        Integer.parseInt(startSplit[1]),
                        Integer.parseInt(startSplit[2]));

        }


        ConnectionManager manager = ConnectionManager.defaultManager();
        ArrayList<DatabaseTransaction> transactions;
        if (endDate != null) {
            transactions = manager.loadTransactions(startDate, endDate);
        } else {
            transactions = manager.loadTransactions(startDate);
        }

        if (transactions != null) {
            session.setAttribute("requestedStart", startDate);
            if (endDate != null) {
                session.setAttribute("requestedEnd", endDate);
                session.setAttribute("usedEnd", true);
            } else {
                session.setAttribute("usedEnd", false);
            }

            factory.setLoadedTransactions(transactions);
            request.setAttribute("successMessage", "Transactions successfully loaded.");
        } else {
            request.setAttribute("errorMessage", "No transactions found for this date range.");
        }
    }

    public void modifyTransaction() throws Exception {
        int id = Integer.parseInt(request.getParameter("to_modify"));
        String date = request.getParameter("date");
        double amount = Double.parseDouble(request.getParameter("amount"));
        String account = request.getParameter("account");
        String type = request.getParameter("type");
        String tag = request.getParameter("tag");

        ConnectionManager manager = ConnectionManager.defaultManager();
        manager.modifyTransaction(id, date, amount, account, type, tag);
        calculateOverview(false);
    }
}
