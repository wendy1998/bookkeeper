package nl.wendy.bookkeeper.controllers.overview;

import nl.wendy.bookkeeper.FileProcessing.FileProcessor;
import nl.wendy.bookkeeper.controllers.AbstractController;
import nl.wendy.bookkeeper.objects.InfoFactory;
import nl.wendy.bookkeeper.objects.Transaction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PeriodicController extends AbstractController {

    public PeriodicController(HttpServletRequest request, HttpServletResponse response) {
        super(request, response);
    }

    public void createPeriodicTransactions() throws Exception {
        HttpSession session = this.request.getSession();
        Map<String, String[]> parameterMap = request.getParameterMap();

        String[] descriptions = parameterMap.get("description");
        String[] amounts = parameterMap.get("amount");

        List<Object> periodicTransactions = Transaction.setupPeriodicTransactions(amounts, descriptions);
        int skipped = Integer.parseInt(String.valueOf(periodicTransactions.get(0))) - 1;
        HashMap<String, String> transactions = (HashMap<String, String>) periodicTransactions.get(1);

        FileProcessor.saveJsonFiles(transactions, true);
        InfoFactory inf = (InfoFactory) session.getAttribute("infoFactory");
        inf.setPeriodicTransactions(transactions);
        if (skipped > 0) {
            request.setAttribute("errorMessage", skipped +
                    " transaction(s) couldn't be saved because either " +
                    "the amount or the description or both were missing, these were skipped.");
        } else {
            request.setAttribute("successMessage", "Periodic transactions successfully saved.");
        }
    }
}
