package nl.wendy.bookkeeper.servlets;

import nl.wendy.bookkeeper.dao.ConnectionManager;
import nl.wendy.bookkeeper.objects.DatabaseTransaction;
import nl.wendy.bookkeeper.objects.InfoFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@WebServlet(name = "TransactionsServlet", urlPatterns = "/transactions")
public class TransactionsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("addTransaction.jsp");
        try {
            HttpSession session = request.getSession();
            if (session.getAttribute("infoFactory") == null) {
                session.setAttribute("infoFactory", new InfoFactory());
            }
            // TODO: modify transaction
        } catch (Exception e) {
            request.setAttribute("errorMessage", "Something went wrong: " + e.getMessage());
        }

        view.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("addTransaction.jsp");
        try {
            HttpSession session = request.getSession();
            if (session.getAttribute("infoFactory") == null) {
                session.setAttribute("infoFactory", new InfoFactory());
            }
            session.removeAttribute("transaction");

            List<String> required = List.of("date", "amount", "account", "type", "tag");
            Map<String, String[]> parameterMap = request.getParameterMap();
            for (String str: required) {
                if (!parameterMap.containsKey(str)) {
                    request.setAttribute("errorMessage",
                            "One or more required parameters weren't given, use the form to add transaction.");
                    view.forward(request, response);
                    return;
                }
            }

            String date = request.getParameter("date");
            double amount = Double.parseDouble(request.getParameter("amount"));
            String account = request.getParameter("account");
            String type = request.getParameter("type");
            String tag = request.getParameter("tag");


            ConnectionManager connectionManager = ConnectionManager.defaultManager();
            // check for possible duplicates if requested
            if (parameterMap.containsKey("checkDup")) {
                ArrayList<DatabaseTransaction> duplicates = connectionManager.checkForDuplicates(amount, account);
                if (duplicates != null) {
                    request.setAttribute("duplicates", duplicates);
                    view.forward(request, response);
                    return;
                }
            }

            connectionManager.addNewTransaction(date, amount, account, type, tag);
            request.setAttribute("successMessage", "Transaction successfully added to the database.");
        } catch (Exception e) {
            request.setAttribute("errorMessage",
                    "Something went wrong: " + e.getMessage());
        }

        view.forward(request, response);
    }
}
