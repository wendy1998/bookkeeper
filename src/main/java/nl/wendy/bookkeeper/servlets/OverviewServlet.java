package nl.wendy.bookkeeper.servlets;

import nl.wendy.bookkeeper.controllers.AbstractController;
import nl.wendy.bookkeeper.controllers.overview.OverviewController;
import nl.wendy.bookkeeper.controllers.overview.PeriodicController;
import nl.wendy.bookkeeper.controllers.overview.SnapshotController;
import nl.wendy.bookkeeper.objects.InfoFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "OverviewServlet", urlPatterns = "/overview")
public class OverviewServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("index.jsp");
        AbstractController controller = null;
        try {
            HttpSession session = request.getSession();
            if (session.getAttribute("infoFactory") == null) {
                session.setAttribute("infoFactory", new InfoFactory());
            }

            String origin = request.getParameter("origin");
            String action = request.getParameter("action");

            if (origin == null || action == null) {
                request.setAttribute("errorMessage", "invalid request, nothing was done.");
                view.forward(request, response);
                return;
            }

            switch (origin) {
                case "snapshot":
                    controller = new SnapshotController(request, response);
                    switch (action) {
                        case "save":
                            ((SnapshotController) controller).saveNewSnapshot();
                            break;
                        case "hide":
                            // TODO: hide from snapshot
                            ((SnapshotController) controller).hideFromSnapshot();
                            break;
                        case "add":
                            // TODO: add new (snapshot) type
                            ((SnapshotController) controller).addTypeToSnapshot();
                            break;
                        default:
                            request.setAttribute("errorMessage", "invalid request, nothing was done.");
                    }
                    break;
                case "periodic":
                    controller = new PeriodicController(request, response);
                    switch (action) {
                        case "create":
                            ((PeriodicController) controller).createPeriodicTransactions();
                            break;
                        case "start-modify":
                            request.setAttribute("modify", true);
                            break;
                        default:
                            request.setAttribute("errorMessage", "invalid request, nothing was done.");
                    }
                    break;
                case "table":
                    controller = new OverviewController(request, response);
                    switch (action) {
                        case "start-modify":
                            String id = request.getParameter("to_modify");
                            request.setAttribute("to_modify", id);
                            break;
                        case "modify":
                            ((OverviewController) controller).modifyTransaction();
                            break;
                        default:
                            request.setAttribute("errorMessage", "invalid request, nothing was done.");
                    }
                    break;
                default:
                    request.setAttribute("errorMessage", "invalid request, nothing was done.");
            }
        } catch (Exception e) {
            request.setAttribute("errorMessage", "Something went wrong: " + e.getMessage());
        }

        if (controller != null) {
            request = controller.getRequest();
            response = controller.getResponse();
        }
        view.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("index.jsp");
        OverviewController controller = null;
        try {
            HttpSession session = request.getSession();
            if (session.getAttribute("infoFactory") == null) {
                session.setAttribute("infoFactory", new InfoFactory());
            }
            if (request.getParameter("from-form") != null) {
                controller = new OverviewController(request, response);
                controller.calculateOverview();
            }
        } catch (Exception e) {
            request.setAttribute("errorMessage", "Something went wrong: " + e.getMessage());
        }

        if (controller != null) {
            request = controller.getRequest();
            response = controller.getResponse();
        }
        view.forward(request, response);
    }
}
