package nl.wendy.bookkeeper.servlets;

import nl.wendy.bookkeeper.FileProcessing.FileProcessor;
import nl.wendy.bookkeeper.objects.InfoFactory;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "FileHandlingServlet", urlPatterns = "/file")
public class FileHandlingServlet extends javax.servlet.http.HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("fileprocessor.jsp");
        try {
            HttpSession session = request.getSession();
            if (session.getAttribute("infoFactory") == null) {
                session.setAttribute("infoFactory", new InfoFactory());
            }
        } catch (Exception e) {
            request.setAttribute("errorMessage", "Something went wrong: " + e.getMessage());
        }

        int maxMemSize = 5000 * 1024;

        // Verify the content type
        String contentType = request.getContentType();
        ArrayList<String> unprocessed = new ArrayList<>();
        if ((contentType.contains("multipart/form-data"))) {
            try {
                Context env = (Context) new InitialContext().lookup("java:comp/env");
                String tempPath = (String) env.lookup("file-temp");

                DiskFileItemFactory factory = new DiskFileItemFactory();
                // maximum size that will be stored in memory
                factory.setSizeThreshold(maxMemSize);

                // Location to save data that is larger than maxMemSize.
                factory.setRepository(
                        new File(tempPath)
                );

                // Create a new file upload handler
                ServletFileUpload upload = new ServletFileUpload(factory);

                // Parse the request to get file items.
                List<FileItem> fileItems = upload.parseRequest(request);

                // Process the uploaded file items
                unprocessed = FileProcessor.saveTransactionsFiles(fileItems);
                HttpSession session = request.getSession();
                session.setAttribute("files", FileProcessor.getFiles());
            } catch(Exception ex) {
                System.out.println(ex.getMessage());
                request.setAttribute("errorMessage",
                        "Something went wrong: " + ex.getMessage()
                );
            } finally {
                // list unprocessed files if there are any.
                if (unprocessed.size() > 0) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Not all files were processed, unprocessed files:");
                    for (String fileName: unprocessed) {
                        sb.append("\n").append(fileName);
                    }

                    if (request.getAttribute("errorMessage") != null) {
                        sb.append("\n");
                        sb.append(request.getAttribute("errorMessage").toString());
                    }
                    request.setAttribute("errorMessage", sb.toString());
                    view = request.getRequestDispatcher("index.jsp");
                }
                else if (request.getAttribute("errorMessage") == null) {
                    request.setAttribute("successMessage", "All files processed.");
                }
            }
        } else {
            request.setAttribute("errorMessage", "No Files processed");
        }
        view.forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("fileprocessor.jsp");
        try {
            HttpSession session = request.getSession();
            if (session.getAttribute("infoFactory") == null) {
                session.setAttribute("infoFactory", new InfoFactory());
            }
        } catch (Exception e) {
            request.setAttribute("errorMessage", "Something went wrong: " + e.getMessage());
        }

        String action = request.getParameter("action");
        if (action == null) {
            request.setAttribute("errorMessage", "No action given, nothing was done.");
        } else {
            try {
                switch (action) {
                    case "clean":
                        FileProcessor.cleanFiles();
                        request.setAttribute("successMessage", "Files have been cleaned out.");
                    case "load":
                        HttpSession session = request.getSession();
                        session.setAttribute("files", FileProcessor.getFiles());
                        if (request.getAttribute("successMessage") == null) {
                            request.setAttribute("successMessage", "Files have been loaded successfully.");
                        }
                        break;
                    default:
                        request.setAttribute("errorMessage", "Invalid action, nothing was done.");
                        break;
                }
            } catch (Exception e) {
                request.setAttribute("errorMessage", "Something went wrong: " + e.getMessage());
            }
        }

        view.forward(request, response);
    }
}
