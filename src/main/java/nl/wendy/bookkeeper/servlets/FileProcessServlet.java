package nl.wendy.bookkeeper.servlets;

import nl.wendy.bookkeeper.FileProcessing.CsvProcessor;
import nl.wendy.bookkeeper.FileProcessing.FileProcessor;
import nl.wendy.bookkeeper.objects.DatabaseTransaction;
import nl.wendy.bookkeeper.objects.InfoFactory;
import nl.wendy.bookkeeper.objects.MemoryTransaction;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@WebServlet(name = "FileProcessServlet", urlPatterns = "/process")
public class FileProcessServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        RequestDispatcher view = request.getRequestDispatcher("fileprocessor.jsp");
        try {
            if (session.getAttribute("infoFactory") == null) {
                session.setAttribute("infoFactory", new InfoFactory());
            }
            var transactions = session.getAttribute("transactions");
            if (transactions == null) {
                request.setAttribute("errorMessage", "no transactions found in memory");
            } else {
                String index = request.getParameter("index");
                if (index == null && session.getAttribute("transaction") == null) {
                    request.setAttribute("errorMessage", "no transaction identifier given, " +
                            "no transaction could be found.");
                } else {
                    ArrayList<MemoryTransaction> transactionsList = (ArrayList<MemoryTransaction>) transactions;
                    String action = request.getParameter("action");
                    if (action == null) {
                        request.setAttribute("errorMessage", "no action given, " +
                                "so nothing to do.");
                    } else {
                        switch (action) {
                            case "database":
                                Object transaction = session.getAttribute("transaction");
                                if (transaction != null && index == null) {
                                    String type = request.getParameter("type");
                                    String tag = request.getParameter("tag");
                                    MemoryTransaction toProcess = (MemoryTransaction) transaction;
                                    toProcess.addToDatabase(type, tag);
                                    request.setAttribute("successMessage",
                                            "Transaction successfully added to database");
                                    session.removeAttribute("transaction");
                                } else {
                                    MemoryTransaction toProcess = transactionsList.get(Integer.parseInt(index));
                                    ArrayList<DatabaseTransaction> possibleDups = toProcess.checkForDuplicates();
                                    if (possibleDups != null) {
                                        request.setAttribute("duplicates", possibleDups);
                                    }
                                    session.setAttribute("transaction", toProcess);
                                }
                                break;
                            case "remove":
                                session.removeAttribute("transaction");
                                if (index.equals("all")) {
                                    session.removeAttribute("transactions");
                                } else {
                                    transactionsList.remove(Integer.parseInt(index));
                                    session.setAttribute("transactions", transactionsList);
                                }
                                request.setAttribute("successMessage",
                                        "Transaction(s) successfully removed from memory");
                                break;
                            case "inDatabase":
                                session.removeAttribute("transaction");
                                MemoryTransaction toProcess = transactionsList.get(Integer.parseInt(index));
                                toProcess.setInDatabase(true);
                                break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            request.setAttribute("errorMessage", "Something went wrong: " + e.getMessage());
        }
        view.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        try {
            if (session.getAttribute("infoFactory") == null) {
                session.setAttribute("infoFactory", new InfoFactory());
            }
        } catch (Exception e) {
            request.setAttribute("errorMessage", "Something went wrong: " + e.getMessage());
        }

        Map<String, String[]> parameters = request.getParameterMap();
        RequestDispatcher view = request.getRequestDispatcher("fileprocessor.jsp");
        if (!parameters.containsKey("filename")) {
            request.setAttribute("errorMessage", "no file provided to process, nothing will be done");
        } else {
            try {
                session = request.getSession();
                String filename = parameters.get("filename")[0];
                if (parameters.containsKey("process")) {
                    if (!parameters.containsKey("separator")) {
                        request.setAttribute("errorMessage",
                                "use the form to request file processing. " +
                                        "No processing done.");
                        view.forward(request, response);
                    } else {
                        CsvProcessor processor = new CsvProcessor();
                        String separator = parameters.get("separator")[0];
                        ArrayList<MemoryTransaction> transactions = processor.loadTransactions(filename, separator);
                        Object oldTransactions = session.getAttribute("transactions");
                        if (oldTransactions != null) {
                            ArrayList<MemoryTransaction> combined =
                                    (ArrayList<MemoryTransaction>) oldTransactions;
                            combined.addAll(transactions);
                            List<MemoryTransaction> newTransactions =
                                    combined.stream().distinct().collect(Collectors.toList());
                            session.setAttribute("transactions", newTransactions);
                        } else {
                            session.setAttribute("transactions", transactions);
                        }
                    }
                }

                if (parameters.containsKey("remove")) {
                    FileProcessor.removeFile(filename);
                    session.setAttribute("files", FileProcessor.getFiles());
                }
            } catch (Exception e) {
                request.setAttribute("errorMessage", "Something went wrong: " + e.getMessage());
            }
        }
        view.forward(request, response);
    }
}
